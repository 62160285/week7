
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author akemi
 */
public class UserService {

    private static ArrayList<User> userList = null;

    static {
        userList = new ArrayList<>();
        //Mook Data
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
        userList.add(new User("user2", "password"));
    }

    // Create (C)
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    //delete (D)
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //Read (R)
    public static ArrayList<User> getUsers() {
        return userList;
    }
    public static User getUser(int index){
        return userList.get(index);
    }
    //Update (U)
    public static boolean updateUser(int undex, User user) {
        userList.set(undex, user);
        return true;
    }

    //File
    public static void save() {

    }

    public static void load() {

    }
}
